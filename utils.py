import logging

import pymysql
import requests
from bs4 import BeautifulSoup

import app


def assert_utils(self,response,status_code,status,desc):
    self.assertEqual(status_code, response.status_code)
    self.assertEqual(status, response.json().get("status"))
    self.assertEqual(desc, response.json().get("description"))

def request_third_api(form_data):
    # 解析form表单中的内容，并提取第三方请求的参数
    soup = BeautifulSoup(form_data, "html.parser")
    third_url = soup.form['action']
    logging.info("third request url = {}".format(third_url))
    data = {}
    for input in soup.find_all('input'):
        data.setdefault(input['name'], input['value'])
    logging.info("third request data = {}".format(data))
    # 发送第三方请求
    response = requests.post(third_url, data=data)
    return response

class DButils:
    @classmethod
    def  get_conn(cls,db_name):
        conn = pymysql.connect(app.DB_RUL,app.DB_USERNAME,app.DB_PASSWORD,db_name,autocommit=True)

    @classmethod
    def close(cls, cursor,conn):
        if cursor:
            cursor.close()
        if conn:
            conn.close()

    @classmethod
    def  delete(cls,db_name,sql):
        try:
            conn = cls.get_conn(db_name)
            curses = conn.cursor
            curses.execute(sql)
        except Exception as e:
            conn.rollback()
        finally:
            cls.close()

